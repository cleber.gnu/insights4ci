Insights4CI
-----------

.. container::

   |Stability| |Contributions welcome| |Python| |VueJS3| |License|

   Insights4CI is **an experimental project** intended to provide
   project and runners maintainers, developers, and QE teams with
   insights on CI/CD pipelines, helping them better understand their
   test pipelines.

   `Installation <#installation>`__ • `How to
   Contribute <#contributing>`__ •

.. figure:: /_static/print.png
   :alt: Test result page (WiP)

   Work in Progress Interface


Installing
==========

Production deployment
~~~~~~~~~~~~~~~~~~~~~

You can deploy it in your OCP or K8S cluster with the following
commands:

First, create the ImageStreams and BuildConfigs. This step must be
executed only once in your cluster:

::

   $ oc apply -f ./deployment/configs/buildconfigs/api-server.yaml
   $ oc apply -f ./deployment/configs/buildconfigs/web-ui.yaml

Now, for each project you have you can onboard in a specific application
group, passing the right parameters with the ``-p`` option:

::

   $ oc new-app -f ./deployment/templates/insights4ci-project-onboarding.yaml \
     -p APPLICATION_GROUP=myproject

There are a few parameters inside the template, but for now, only
``APPLICATION_GROUP`` is required. Everything else it will be generated
if skip them.

Development deployment
~~~~~~~~~~~~~~~~~~~~~~

If you would like to contribute with this project, it is better to have
it running locally with debug mode, development requirements installed
and “watch feature” enabled.

Visit HACKING.md and CONTRIBUTING.md files for details.

Populating
==========

TODO: Improve here

Contributing
============

This project has just started, so there is no better time for you to
collaborate.

Please, visit the ``CONTRIBUTING.md`` file for how to contribute and how
to deploy insights4ci with other methods.

License
=======

This project is licensed under the terms of the GPL Open Source license
(v2) and is available for free. For details, visit the ``LICENSE`` file.

.. |Stability| image:: https://img.shields.io/badge/stability-experimental-red.svg
.. |Contributions welcome| image:: https://img.shields.io/badge/contributions-welcome-orange.svg
.. |Python| image:: https://img.shields.io/badge/Python-v3.6+-blue.svg
.. |VueJS3| image:: https://img.shields.io/badge/VueJS-v3-brightgreen.svg
.. |License| image:: https://img.shields.io/badge/license-GPLv2-blue.svg
